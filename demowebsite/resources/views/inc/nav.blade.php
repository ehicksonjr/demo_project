<!-- Static navbar -->
<header>
  <nav class="navbar navbar-default">
    <div class="container-fluid">
      <div id="navbar" class="">
        <ul class="nav navbar-nav">
          <li class="{{ (request()->is('/')) ? 'active' : '' }}"><a href="/">Home</a></li>
          <li class="{{ (request()->is('order')) ? 'active' : '' }}"><a href="#">Order</a></li>
          <li class="{{ (request()->is('blog')) ? 'active' : '' }}"><a href="#">Blog</a></li>
          <li class="{{ (request()->is('about')) ? 'active' : '' }}"><a href="/about">About</a></li>
          <li class="{{ (request()->is('contact')) ? 'active' : '' }}"><a href="/contact">Contact</a></li>
        </ul>
      </div><!--/.nav-collapse -->
    </div><!--/.container-fluid -->
  </nav>
</header>
