

{!! Form::open(['url' => 'contact/thank-you' , 'method' => 'POST' , 'class' => 'form' , 'id' => 'form']) !!}
    <div class="form-group">
      <div class="col-lg-12 mb-3">
        <h3>Contact Form</h3>
        <div id="alert" class="alert alert-dismissible alert-danger">
          <button type="button" id="close" class="close" data-dismiss="alert">&times;</button>
          <strong>Please enter the same email twice</strong>
        </div>
      </div>
    </div>
    <div class="form-group">
      <div class="col-lg-12 mb-3">
        {{Form::text('email','', ['id' => 'email' , 'class' => 'form-control email' , 'placeholder' => 'Enter email' , 'required'])}}
      </div>
    </div>
    <div class="form-group form-confirm">
      <div class="col-lg-12 mb-3">
        {{Form::text('email2','', ['id' => 'confirm-email' ,'class' => 'form-control confirm' , 'placeholder' => 'Confirm email' , 'required'])}}
      </div>
    </div>
    <p>Please enter the same email address again</p>
    <div class="form-group">
      <div class="col-lg-12 mb-3">
        {{Form::textArea('textArea','', ['class' => 'form-control' , 'placeholder' => 'Please enter comment here'])}}
      </div>
    </div>

    <div class="form-group">
      <div class="col-lg-12 mb-3">
      <button id="submit" class="btn btn-primary btn-lg btn-block" type="submit">Submit</button>

      </div>
    </div>
{!! Form::close() !!}
